package hr.ferit.bernardkekelic.universalconverter

interface ConvInteractionListener {
    fun onOpenSpinnerIntent(id: Int)
}