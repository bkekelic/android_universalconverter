package hr.ferit.bernardkekelic.universalconverter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_conv.view.*

class ConvAdapter(
    convs: MutableList<Conv>,
    convListener: ConvInteractionListener
): RecyclerView.Adapter<ConvHolder>() {

    private val convs: MutableList<Conv>
    private val convListener: ConvInteractionListener

    init{
        this.convs = mutableListOf()
        this.convs.addAll(convs)
        this.convListener = convListener
    }
    fun refreshData(convs: MutableList<Conv>){
        this.convs.clear()
        this.convs.addAll(convs)
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int = convs.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConvHolder {
        val convView = LayoutInflater.from(parent.context).inflate(R.layout.item_conv, parent, false)
        val convHolder = ConvHolder(convView)
        return convHolder
    }

    override fun onBindViewHolder(holder: ConvHolder, position: Int) {
        val conv = convs[position]
        holder.bind(conv, convListener)
    }





}

class ConvHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    fun bind(conv: Conv, convListener: ConvInteractionListener){
        itemView.itemConvPicture.setImageResource(conv.pictureId)

        itemView.setOnClickListener{convListener.onOpenSpinnerIntent(conv.id)}

    }

}
