package hr.ferit.bernardkekelic.universalconverter

import android.content.Intent
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpUi()
    }

    private fun setUpUi() {
        convsDiplay.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        convsDiplay.itemAnimator = DefaultItemAnimator()
        convsDiplay.addItemDecoration(DividerItemDecoration(this, RecyclerView.VERTICAL))
        displayData()
    }

    private fun displayData() {
        val convListener = object: ConvInteractionListener{

            override fun onOpenSpinnerIntent(id: Int) {
                openNewIntent(id)
            }
        }
        convsDiplay.adapter = ConvAdapter(ConvRepository.convs, convListener)
    }

    private fun openNewIntent(id: Int) {
        val spinnerIntent: Intent = Intent(this, SpinnerActivity::class.java)
        spinnerIntent.putExtra(SpinnerActivity.SPINNER_KEY, id)
        startActivity(spinnerIntent)
    }
}
