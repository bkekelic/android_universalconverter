package hr.ferit.bernardkekelic.universalconverter

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {

    companion object {
        const val ORIGINAL_VALUE_KEY: String = "original"
        const val RESULT_VALUE_KEY: String = "result"
        const val INPUT_KEY: String = "input"
        const val DEFAULT_INPUT: Float = 0F
        const val CHOSEN_VIEW: String = "chosenView"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("RESACT", "hello")


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        setUpUi()
    }

    private fun setUpUi() {
        var original: String = intent?.getStringExtra(ORIGINAL_VALUE_KEY) ?: "Nothing received as original"
        var result: String = intent?.getStringExtra(RESULT_VALUE_KEY) ?: "Nothing received as result"
        var num: Float = intent?.getFloatExtra(INPUT_KEY, DEFAULT_INPUT) ?: DEFAULT_INPUT
        var chosenView: Int = intent?.getIntExtra(CHOSEN_VIEW, 0) ?: 0
        Log.d("RESACT", "orig = $original, res = $result, val = $num")

        calculateResult(chosenView, original, result, num)

        resultActivity_Original_tw.text = original
        resultActivity_InputValue_tw.text = num.toString()

        resultActivity_Converted_tw.text = result
    }

    private fun calculateResult(chosenView: Int, calculateFrom: String, calculateTo: String, inputNum: Float) {


        var result: Float = 0F

        //Odabran je pretvornik TEMPERATURE
        if(chosenView == 1){

            val arrayTemperature = resources.getStringArray(R.array.temperatureConverter_array)

            //IZ CELZIJA
            if(calculateFrom == arrayTemperature[0]){

                when (calculateTo) {
                    //u celzij
                    arrayTemperature[0] -> result = inputNum
                    //u fahrenheit
                    arrayTemperature[1] -> result = inputNum * 1.8F + 32
                    //u kelvin
                    arrayTemperature[2] -> result = inputNum + 273.15F
                }

            }
            //IZ FAHRENHIET
            else if(calculateFrom == arrayTemperature[1]){

                when (calculateTo) {
                    //u celzij
                    arrayTemperature[0] -> result = (inputNum - 32F)/1.8F
                    //u fahrenheit
                    arrayTemperature[1] -> result = inputNum
                    //u kelvin
                    arrayTemperature[2] -> result = ((inputNum - 32F)/1.8F + 273.15F)
                }

            }
            //IZ KELVIN
            else if(calculateFrom == arrayTemperature[2]){

                when (calculateTo) {
                    //u celzij
                    arrayTemperature[0] -> result = inputNum - 273.15F
                    //u fahrenheit
                    arrayTemperature[1] -> result = ((inputNum-273.15F)*1.8F)+32F
                    //u kelvin
                    arrayTemperature[2] -> result = inputNum
                }

            }


        }
        //Odabran je pretvornik TEZINE
        else if(chosenView == 2){

            val arrayWeight = resources.getStringArray(R.array.weightConverter_array)

            when (calculateFrom) {
                //IZ KILOGRAMA
                arrayWeight[0] -> when (calculateTo) {
                    //u kilograme
                    arrayWeight[0] -> result = inputNum
                    //u pounds
                    arrayWeight[1] -> result = inputNum * 2.2046F
                    //u ounces
                    arrayWeight[2] -> result = inputNum * 35.274F
                    //u stones
                    arrayWeight[3] -> result = inputNum * 0.15747F
                }
                //IZ POUNDS
                arrayWeight[1] -> when (calculateTo) {
                    //u kilograme
                    arrayWeight[0] -> result = inputNum / 2.2046F
                    //u pounds
                    arrayWeight[1] -> result = inputNum
                    //u ounces
                    arrayWeight[2] -> result = inputNum * 16F
                    //u stones
                    arrayWeight[3] -> result = inputNum * 0.071429F
                }
                //IZ OUNCES
                arrayWeight[2] -> when (calculateTo) {
                    //u kilograme
                    arrayWeight[0] -> result = inputNum / 35.274F
                    //u pounds
                    arrayWeight[1] -> result = inputNum * 0.062500F
                    //u ounces
                    arrayWeight[2] -> result = inputNum
                    //u stones
                    arrayWeight[3] -> result = inputNum * 0.0044643F
                }
                //IZ STONES
                arrayWeight[3] -> when (calculateTo) {
                    //u kilograme
                    arrayWeight[0] -> result = inputNum / 0.15747F
                    //u pounds
                    arrayWeight[1] -> result = inputNum * 14F
                    //u ounces
                    arrayWeight[2] -> result = inputNum * 224F
                    //u stones
                    arrayWeight[3] -> result = inputNum
                }
            }

        }
        //Odabran je pretvornik UDALJENOSTI
        else if(chosenView == 3){

            val arrayDistance = resources.getStringArray(R.array.distanceConverter_array)

            when (calculateFrom) {
                //FROM MILES
                arrayDistance[0] -> when (calculateTo) {
                    //to miles
                    arrayDistance[0] -> result = inputNum
                    //to kilometers
                    arrayDistance[1] -> result = inputNum / 0.62137F
                    //to meters
                    arrayDistance[2] -> result = inputNum / 0.00062137F
                    //to feet
                    arrayDistance[3] -> result = inputNum * 5280.0F
                    //to inches
                    arrayDistance[4] -> result = inputNum * 63360F
                    //to centimeters
                    arrayDistance[5] -> result = inputNum / 0.0000062137F
                }
                //FROM KILOMETERS
                arrayDistance[1] -> when (calculateTo) {
                    //to miles
                    arrayDistance[0] -> result = inputNum * 0.62137F
                    //to kilometers
                    arrayDistance[1] -> result = inputNum
                    //to meters
                    arrayDistance[2] -> result = inputNum * 1000
                    //to feet
                    arrayDistance[3] -> result = inputNum * 3280.8F
                    //to inches
                    arrayDistance[4] -> result = inputNum * 39370F
                    //to centimeters
                    arrayDistance[5] -> result = inputNum / 0.00001F
                }
                //FROM METERS
                arrayDistance[2] -> when (calculateTo) {
                    //to miles
                    arrayDistance[0] -> result = inputNum * 0.00062137F
                    //to kilometers
                    arrayDistance[1] -> result = inputNum / 0.001F
                    //to meters
                    arrayDistance[2] -> result = inputNum
                    //to feet
                    arrayDistance[3] -> result = inputNum * 3.2808F
                    //to inches
                    arrayDistance[4] -> result = inputNum * 39.370F
                    //to centimeters
                    arrayDistance[5] -> result = inputNum * 100F
                }
                //FROM FEET
                arrayDistance[3] -> when (calculateTo) {
                    //to miles
                    arrayDistance[0] -> result = inputNum * 0.00018939F
                    //to kilometers
                    arrayDistance[1] -> result = inputNum / 3280.8F
                    //to meters
                    arrayDistance[2] -> result = inputNum / 3.2808F
                    //to feet
                    arrayDistance[3] -> result = inputNum
                    //to inches
                    arrayDistance[4] -> result = inputNum * 12F
                    //to centimeters
                    arrayDistance[5] -> result = inputNum / 0.032808F
                }
                //FROM INCHES
                arrayDistance[4] -> when (calculateTo) {
                    //to miles
                    arrayDistance[0] -> result = inputNum * 0.000015783F
                    //to kilometers
                    arrayDistance[1] -> result = inputNum / 39370F
                    //to meters
                    arrayDistance[2] -> result = inputNum / 39.370F
                    //to feet
                    arrayDistance[3] -> result = inputNum * 0.083333F
                    //to inches
                    arrayDistance[4] -> result = inputNum
                    //to centimeters
                    arrayDistance[5] -> result = inputNum / 0.39370F
                }
                //FROM CENTIMETERS
                arrayDistance[5] -> when (calculateTo) {
                    //to miles
                    arrayDistance[0] -> result = inputNum * 0.0000062137F
                    //to kilometers
                    arrayDistance[1] -> result = inputNum / 100000F
                    //to meters
                    arrayDistance[2] -> result = inputNum / 100F
                    //to feet
                    arrayDistance[3] -> result = inputNum * 0.032808F
                    //to inches
                    arrayDistance[4] -> result = inputNum * 0.39370F
                    //to centimeters
                    arrayDistance[5] -> result = inputNum
                }

            }

        }
        //Odabran je pretvornik PODATAKA
        else if(chosenView == 4) {

            val arrayDistance = resources.getStringArray(R.array.dataConverter_array)

            when (calculateFrom) {
                //FROM BIT
                arrayDistance[0] -> when (calculateTo) {
                    //to bit
                    arrayDistance[0] -> result = inputNum
                    //to byte
                    arrayDistance[1] -> result = inputNum / 8F
                    //to kilobyte
                    arrayDistance[2] -> result = inputNum / 8192F
                    //to megabyte
                    arrayDistance[3] -> result = inputNum / 8388608F
                    //to gigabyte
                    arrayDistance[4] -> result = inputNum / 8589934593F
                    //to terabyte
                    arrayDistance[5] -> result = inputNum / 8796093022208F
                }
                //FROM BYTE
                arrayDistance[1] -> when (calculateTo) {
                    //to bit
                    arrayDistance[0] -> result = inputNum * 8
                    //to byte
                    arrayDistance[1] -> result = inputNum
                    //to kilobyte
                    arrayDistance[2] -> result = inputNum / 1024F
                    //to megabyte
                    arrayDistance[3] -> result = inputNum / 1048576F
                    //to gigabyte
                    arrayDistance[4] -> result = inputNum / 1073741824F
                    //to terabyte
                    arrayDistance[5] -> result = inputNum / 1099511627776F
                }
                //FROM KILOBYTE
                arrayDistance[2] -> when (calculateTo) {
                    //to bit
                    arrayDistance[0] -> result = inputNum * 8192F
                    //to byte
                    arrayDistance[1] -> result = inputNum * 1024F
                    //to kilobyte
                    arrayDistance[2] -> result = inputNum
                    //to megabyte
                    arrayDistance[3] -> result = inputNum / 1024F
                    //to gigabyte
                    arrayDistance[4] -> result = inputNum / 1048576F
                    //to terabyte
                    arrayDistance[5] -> result = inputNum / 1073741824F
                }
                //FROM MEGABYTE
                arrayDistance[3] -> when (calculateTo) {
                    //to bit
                    arrayDistance[0] -> result = inputNum * 8388608F
                    //to byte
                    arrayDistance[1] -> result = inputNum * 1048576F
                    //to kilobyte
                    arrayDistance[2] -> result = inputNum * 1024F
                    //to megabyte
                    arrayDistance[3] -> result = inputNum
                    //to gigabyte
                    arrayDistance[4] -> result = inputNum / 1024F
                    //to terabyte
                    arrayDistance[5] -> result = inputNum / 1048576F
                }
                //FROM MEGABYTE
                arrayDistance[4] -> when (calculateTo) {
                    //to bit
                    arrayDistance[0] -> result = inputNum * 8589934593F
                    //to byte
                    arrayDistance[1] -> result = inputNum * 1073741824F
                    //to kilobyte
                    arrayDistance[2] -> result = inputNum * 1048576F
                    //to megabyte
                    arrayDistance[3] -> result = inputNum * 1024F
                    //to gigabyte
                    arrayDistance[4] -> result = inputNum
                    //to terabyte
                    arrayDistance[5] -> result = inputNum / 10224F
                }
                //FROM TERABYTE
                arrayDistance[5] -> when (calculateTo) {
                    //to bit
                    arrayDistance[0] -> result = inputNum * 8796093022208F
                    //to byte
                    arrayDistance[1] -> result = inputNum * 1099511627776F
                    //to kilobyte
                    arrayDistance[2] -> result = inputNum * 1073741824F
                    //to megabyte
                    arrayDistance[3] -> result = inputNum * 1048576F
                    //to gigabyte
                    arrayDistance[4] -> result = inputNum * 1024F
                    //to terabyte
                    arrayDistance[5] -> result = inputNum
                }



            }
        }





        resultActivity_CalculatedResult_tw.text = result.toString()
    }
}
