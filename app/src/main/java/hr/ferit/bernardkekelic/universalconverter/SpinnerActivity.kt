package hr.ferit.bernardkekelic.universalconverter

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_spinner.*

class SpinnerActivity : AppCompatActivity(),  AdapterView.OnItemSelectedListener{

    companion object {
        const val SPINNER_KEY: String = "spinner"
        const val DEFAULT_KEY: Int = 0
    }

    private var SPINNER_FROM_ID: String = ""
    private var SPINNER_TO_ID: String = ""

    var spinnerFromSelectedItem: String = ""
    var spinnerToSelectedItem: String = ""

    var chosenView: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spinner)

        setUpUi()
    }

    private fun setUpUi() {

        SPINNER_FROM_ID = spinnerActivity_spinner_ConvertFrom.id.toString()
        SPINNER_TO_ID = spinnerActivity_spinner_ConvertTo.id.toString()

        spinnerActivity_ConvertButton.setOnClickListener{
            if(spinnerActivity_ValueInput_et.text.isNotEmpty() && spinnerActivity_ValueInput_et.text.toString() != "0") openResultActivity()
            else Toast.makeText(this, "Invalid input value", Toast.LENGTH_SHORT).show()
        }

        val receivedId = intent?.getIntExtra(SPINNER_KEY, DEFAULT_KEY) ?: DEFAULT_KEY
        chosenView = receivedId

        //TEMPERATURA
        if(receivedId == 1){
            spinnerActivity_HeadText.text = getString(R.string.spinnerActivity_HeadText_Temperature)
            val convertFromSpinner: Spinner = findViewById(R.id.spinnerActivity_spinner_ConvertFrom)
            val convertToSpinner: Spinner = findViewById(R.id.spinnerActivity_spinner_ConvertTo)

            convertFromSpinner.onItemSelectedListener = this
            convertToSpinner.onItemSelectedListener = this

            ArrayAdapter.createFromResource(
                this,
                R.array.temperatureConverter_array,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                convertFromSpinner.adapter = adapter
            }


            ArrayAdapter.createFromResource(
                this,
                R.array.temperatureConverter_array,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                convertToSpinner.adapter = adapter
            }
        }
        //TEŽINA
        else if(receivedId == 2){
            spinnerActivity_HeadText.text = getString(R.string.spinnerActivity_HeadText_Weight)

            val convertFromSpinner: Spinner = findViewById(R.id.spinnerActivity_spinner_ConvertFrom)
            val convertToSpinner: Spinner = findViewById(R.id.spinnerActivity_spinner_ConvertTo)

            convertFromSpinner.onItemSelectedListener = this
            convertToSpinner.onItemSelectedListener = this

            ArrayAdapter.createFromResource(
                this,
                R.array.weightConverter_array,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                convertFromSpinner.adapter = adapter
            }


            ArrayAdapter.createFromResource(
                this,
                R.array.weightConverter_array,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                convertToSpinner.adapter = adapter
            }
        }
        //UDALJENOST
        else if(receivedId == 3){

            spinnerActivity_HeadText.text = getString(R.string.spinnerActivity_HeadText_Distance)

            val convertFromSpinner: Spinner = findViewById(R.id.spinnerActivity_spinner_ConvertFrom)
            val convertToSpinner: Spinner = findViewById(R.id.spinnerActivity_spinner_ConvertTo)

            convertFromSpinner.onItemSelectedListener = this
            convertToSpinner.onItemSelectedListener = this

            ArrayAdapter.createFromResource(
                this,
                R.array.distanceConverter_array,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                convertFromSpinner.adapter = adapter
            }


            ArrayAdapter.createFromResource(
                this,
                R.array.distanceConverter_array,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                convertToSpinner.adapter = adapter
            }
        }
        //PODACI
        else if(receivedId == 4){

            spinnerActivity_HeadText.text = getString(R.string.spinnerActivity_HeadText_Data)

            val convertFromSpinner: Spinner = findViewById(R.id.spinnerActivity_spinner_ConvertFrom)
            val convertToSpinner: Spinner = findViewById(R.id.spinnerActivity_spinner_ConvertTo)

            convertFromSpinner.onItemSelectedListener = this
            convertToSpinner.onItemSelectedListener = this

            ArrayAdapter.createFromResource(
                this,
                R.array.dataConverter_array,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                convertFromSpinner.adapter = adapter
            }


            ArrayAdapter.createFromResource(
                this,
                R.array.dataConverter_array,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                convertToSpinner.adapter = adapter
            }
        }


    }

    private fun openResultActivity() {

        val resultIntent: Intent = Intent(this, ResultActivity::class.java)
        resultIntent.putExtra(ResultActivity.ORIGINAL_VALUE_KEY, spinnerFromSelectedItem)
        resultIntent.putExtra(ResultActivity.RESULT_VALUE_KEY, spinnerToSelectedItem)
        resultIntent.putExtra(ResultActivity.INPUT_KEY, spinnerActivity_ValueInput_et.text.toString().toFloat())
        resultIntent.putExtra(ResultActivity.CHOSEN_VIEW, chosenView)

        Log.d("myDEBUG", "$spinnerFromSelectedItem  $spinnerToSelectedItem ${spinnerActivity_ValueInput_et.text.toString().toFloat()} $chosenView")
        startActivity(resultIntent)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        if(parent?.id.toString() == SPINNER_FROM_ID){
            spinnerFromSelectedItem = parent?.getItemAtPosition(position).toString()
        }else if(parent?.id.toString() == SPINNER_TO_ID){
            spinnerToSelectedItem = parent?.getItemAtPosition(position).toString()
        }
        Log.d("SPINNER", " ITEM SELECTED: FROM(${parent?.id.toString()}) = $spinnerFromSelectedItem, TO${parent?.id.toString()} = $spinnerToSelectedItem")
    }

}
