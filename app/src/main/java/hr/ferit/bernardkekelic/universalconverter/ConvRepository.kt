package hr.ferit.bernardkekelic.universalconverter

object ConvRepository{

    val convs: MutableList<Conv>

    init{
        convs = retriveConvs()
    }

    private fun retriveConvs(): MutableList<Conv> {
        return mutableListOf(
            Conv(1, R.drawable.temp),
            Conv(2, R.drawable.weight),
            Conv(3, R.drawable.distance),
            Conv(4, R.drawable.data)
        )
    }

    fun remove(id: Int) = convs.removeAll{ convs -> convs.id == id}
    fun get(id:Int): Conv? = convs.find { convs -> convs.id == id}
    fun add(conv: Conv) = convs.add(conv)

}